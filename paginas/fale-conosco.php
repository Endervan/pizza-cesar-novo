<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>



<body class="bg-fale-conosco">



  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->

  <!-- bg edescricao fale conosco  -->
  <div class="container-fluid bg-fale ">
    <div class="row ">
      <div class="container ">
        <div class="row">

          <div class="col-xs-12 top100">
            <div class="descricao-fale ">
              <h1>Queremos ouvir sua opinião</h1>
              <p>Enquanto a massa está sendo preparada, preencha o 
                formulário abaixo e mande-nos suas sugestões e dicas para 
                melhorar ainda mais o que já é perfeito.
              </p>
            </div>


            <!-- contatos fale conosco -->
            <div class="col-xs-6 fale-contatos top60 text-center">
              <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Guará</h1>
                  <h3>QI 33 s/n bl A lj 10</h3>
                  <h4 >(61) 3567-3030</h4>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Sudoeste</h1>
                  <h3>ClSW 302 Bl B s/n LJ75 </h3>
                  <h4 >(61) 3341-1001</h4>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Águas Claras</h1>
                  <h3>Rua 07 Norte Max Mall loja 18 </h3>
                  <h4 >(61) 3877-2200</h4>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Asa Sul</h1>
                  <h3>CLS 404 Bloco A Loja 01</h3>
                  <h4 >(61) 3242-2221</h4>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Lago Sul</h1>
                  <h3>SHIS Qi 17 s/n bl G lj 102, Brasília</h3>
                  <h4 >(61) 3364-0191</h4>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Taguatinga</h1>
                  <h3>QNA 17, lote 13 Taguatinga Norte</h3>
                  <h4 >(61) 3352-0500</h4>
                </div>
              </div>

               <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Gama</h1>
                  <h3>Q 30 s/n lt 48 </h3>
                  <h4 >(61) 3384-0202</h4>
                </div>
              </div>

               <div class="col-xs-6">
                <div class="thumbnail">
                  <h1>Sobradinho</h1>
                  <h3>Q 2 Conj D 3 s/n cj A lt 3/5 loja 8</h3>
                  <h4 >(61) 3567-3030</h4>
                </div>
              </div>


            </div>
            <!-- contatos fale conosco -->

            <!-- formulario fale -->
            <div class="col-xs-6 formulario">
            <h4>Envie um email</h4>
              <form class=" form-inline FormContato top10" role="form" method="post">

                  <div class="row">
                    <div class="col-xs-12 form-group ">
                      <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                      <input type="text" name="nome" class="form-control input-lg fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-12 form-group top30">
                      <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                      <input type="text" name="email" class="form-control input-lg fundo-form input100" placeholder="">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12 top20 form-group top30">
                      <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                      <input type="text" name="telefone" class="form-control input-lg fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-12 top20 form-group top30">
                      <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                      <input type="text" name="assunto" class="form-control input-lg fundo-form input100" placeholder="">
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-xs-12 top20 form-group top30">
                      <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                      <textarea name="mensagem" id="" cols="30" rows="20" class="form-control  fundo-form input100" placeholder=""></textarea>
                    </div>

                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right top30 bottom40">
                    <button type="submit" class="btn btn-vermelho" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>


                </form>
            </div>
            <!-- formulario fale -->

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- bg edescricao fale conosco -->







  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->





</body>

</html>



<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>
