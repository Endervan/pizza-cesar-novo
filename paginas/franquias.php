  <!DOCTYPE html>
  <html lang="pt-br">

  <head>
    <?php require_once('./includes/head.php'); ?>


  </head>



  <body class="bg-franquias">



    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->

    <!-- franquias  -->
    <div class="container-fluid bg-franquias1">
      <div class="row ">

        <div class="container">
          <div class="row">
            <div class="col-xs-12 span12 top35">


              <!-- menu -->
              <div id="tab" class="btn-group" data-toggle="buttons-radio">
                <a href="#prices2" class="btn btn-large btn-info btn-franquias active" data-toggle="tab">APRESENTAÇÃO</a>
                <a href="#features2" class="btn btn-large btn-info btn-franquias" data-toggle="tab">INVESTIMENTO</a>
                <a href="#requests2" class="btn btn-large btn-info btn-franquias" data-toggle="tab">PERFIL DO FRANQUEADO</a>
                <a href="#contact2" class="btn btn-large btn-info btn-franquias"  data-toggle="tab">QUERO SER UM FRANQUEADO</a>
              </div>
              <!-- menu -->



              <div class="tab-content">

                <div class="tab-pane active" id="prices2">
                  <div class="row">
                    <h1>Apresentação</h1>
                    <p class="top50 bottom80">Chega ao mercado a Franquia da marca Pizza Cesar como resultado natural do seu processo de crescimento. O nosso compromisso é formar parceria com empreendedores dinâmicos que buscam obter o melhor retorno de seu investimento e de novas oportunidades para atuarem no setor de alimentação, em todo o território Nacional.

                      Nesses 19 anos de atividades, a Pizza Cesar consolidou-se com eficiência e total confiabilidade na prestação de serviço próprio de tele-entrega, e rodízio de pizzas, hoje é membro da ABF Associação Brasileira de Franchinsing.

                      O Franqueado receberá, o melhor modelo implantado do segmento de pizzarias no Brasil, com o melhor plano de negocios do segmento, e melhor rentabilidade, além da transferência de conhecimento na elaboração e comercialização de seus produtos, suporte técnico personalizado para todos os processos envolvidos na implantação e funcionamento da sua unidade comercial.

                      Venha fazer parte deste grupo de sucesso.
                    </p>
                  </div>
                </div>



                <div class="tab-pane" id="features2">
                  <div class="row">
                    <h1>Investimento</h1>
                    <p class="top50 bottom80">O valor total (estimado) do investimento inicial importa 
                      na quantia de R$490.000,00 (quatrocentos e noventa mil reais),
                      a ser despendido pelo Potencial Franqueado, para fazer jus à aquisição,
                      implantação e entrada em operação da franquia.
                    </p>
                  </div>
                </div>


                <div class="tab-pane" id="requests2">

                  <div class="row">
                    <h1>Perfil do franqueado</h1>
                    <p class="top50">Para participar do sistema de franquia da marca Pizza Cesar é necessário atender, inicialmente, os itens abaixo:</p>
                    <p class="top50">- No mínimo, 25 anos de idade;</p>
                    <p>- Conhecimentos básicos de matemática financeira, administração, e recursos humanos;</p>
                    <p>- Residência na cidade em que concentra os seus negócios, sem restrição de qualquer 
                      natureza junto a instituições públicas ou privadas de crédito;
                    </p>
                    <p>- Reserva de recursos próprios para fazer jus ao investimento inicial que venha a exigir a
                     franquia, e ainda suporte de capital de giro para manutenção do negócio;
                   </p>
                   <p>- Aprovoção da família, esposo(a), filho(a)s, e se for o caso, parentes de convivio diário.</p>
                   <p class="bottom80">- Ficha cadastral aprovada pela Franqueadora.</p>

                 </div>
               </div>

               <!-- formulario -->
               <div class="tab-pane" id="contact2">
                 <div class="row formulario">
                   <div class="col-xs-12">
                    <h1>Perfil do franqueado</h1>
                    <p>Preencha o formulário abaixo com os seus dados.</p>
                    <div class="col-xs-6 top20">
                     <h4 class="left20">Informações Pessoais</h4>
                   </div>
                   <div class="col-xs-6 top20">
                     <h4>Onde pretende montar a loja:</h4>
                   </div>
                 </div>



                 <form class="form-inline col-xs-12 FormCurriculo top20 bottom80" role="form" method="post" enctype="multipart/form-data">

                   <div class="col-xs-6">
                    <div class="col-xs-12 form-group ">
                      <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                      <input type="text" name="nome" class="form-control input-lg fundo-form input100" placeholder="">
                    </div>

                    <div class="col-xs-12 form-group top20">
                     <label class="glyphicon glyphicon-user"> <span>Idade</span></label>
                     <input type="text" name="idade" class="form-control input-lg fundo-form input100" placeholder="">
                   </div>

                   <div class="col-xs-12 form-group top20">
                     <label class="glyphicon glyphicon-home"> <span>Enderenço Residencial</span></label>
                     <input type="text" name="endereco" class="form-control input-lg fundo-form input100" placeholder="">
                   </div>

                   <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-home"> <span>Bairro</span></label>
                    <input type="text" name="bairro" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-globe"> <span>Cidade</span></label>
                    <input type="text" name="cidade" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>


                  <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-globe"> <span>Estado</span></label>
                    <input type="text" name="estado" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top30 form-group">
                    <h4 class="pt60 pb20">contatos</h4>
                    <label class="glyphicon glyphicon-phone"> <span> Telefone Celular</span></label>
                    <input type="text" name="celular" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top30 form-group">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone Fixo</span></label>
                    <input type="text" name="fixo" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>


                  <div class="col-xs-12 form-group top20 ">
                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control input-lg fundo-form input100" placeholder="">
                  </div>  



                </div>



                <div class="col-xs-6">

                  <div class="col-xs-12 form-group">
                    <label class="glyphicon glyphicon glyphicon-ok"> <span>opcao 1</span></label>
                    <input type="text" name="opcao1" class="form-control input-lg fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-12 form-group top20">
                    <label class="glyphicon glyphicon glyphicon-ok"> <span>opcao 2</span></label>
                    <input type="text" name="opcao2" class="form-control input-lg fundo-form input100" placeholder="">
                  </div>




                  <div class="col-xs-12 form-group top20">
                    <label class="glyphicon glyphicon glyphicon-ok"> <span>opcao 3</span></label>
                    <input type="text" name="opcao3" class="form-control input-lg fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top30 form-group">
                  <h4 class="pt40 pb20">Dados profissionais</h4>
                    <label class="glyphicon glyphicon-lock"> <span>Ocupação Atual</span></label>
                    <input type="text" name="ocupacao" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>


                  <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-briefcase"> <span>Empresa</span></label>
                    <input type="text" name="empresa" class="form-control fundo-form input-lg input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-book"> <span>Formacao Escolar</span></label>
                    <input type="text" name="escolar" class="form-control input-lg fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top30 form-group">
                    <h4 class="pt40 pb20">Como conheceu o sistema de franquias Pizza César:</h4>
                    <div class=" form-group">
                      <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                      <textarea name="mensagem" id="" cols="33" rows="10" class="form-control input-lg  fundo-form input100" placeholder=""></textarea>
                    </div>
                  </div>
                  <div class="clearfix"></div>

                  <div class="col-xs-12 text-right top30 bottom40">
                    <button type="submit" class="btn btn-vermelho" name="btn_trabalhe_conosco">
                      ENVIAR
                    </button>
                  </div>

                </div>


                

              </form> 



            </div>
          </div>
          <!-- formulario -->

        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- franquias -->







<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->





</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      celular: {
       validators: {
         phone: {
           country: 'BR',
           message: 'Informe um celular válido.'
         }
       },
     },
     fixo: {
       validators: {
         phone: {
           country: 'BR',
           message: 'Informe um telefone válido.'
         }
       },
     },
     opcao1: {
      validators: {
        notEmpty: {

        }
      }
    },
    opcao2: {
      validators: {
        notEmpty: {

        }
      }
    },
    opcao3: {
      validators: {
        notEmpty: {

        }
      }
    },
    bairro: {
      validators: {
        notEmpty: {

        }
      }
    },
    idade: {
      validators: {
        notEmpty: {

        }
      }
    },
    assunto: {
      validators: {
        notEmpty: {

        }
      }
    },
    ocupacao: {
      validators: {
        notEmpty: {

        }
      }
    },
    empresa: {
      validators: {
        notEmpty: {

        }
      }
    },
    escolar: {
      validators: {
        notEmpty: {

        }
      }
    },
    cidade: {
      validators: {
        notEmpty: {

        }
      }
    },
    estado: {
      validators: {
        notEmpty: {

        }
      }
    },
    cargo: {
      validators: {
        notEmpty: {

        }
      }
    },
    area: {
      validators: {
        notEmpty: {

        }
      }
    },
    curriculo: {
      validators: {
        notEmpty: {
          message: 'Por favor insira seu currículo'
        },
        file: {
          extension: 'doc,docx,pdf,rtf',
          type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                              maxSize: 5*1024*1024,   // 5 MB
                              message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                            }
                          }
                        },
                        mensagem: {
                          validators: {
                            notEmpty: {

                            }
                          }
                        }
                      }
                    });
});
</script>

