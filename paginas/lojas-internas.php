<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>



<body class="bg-lojas">



  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->

  <!-- QUAIS AS LOJAS  -->
  <div class="container-fluid bg-lojas-descricao ">
    <div class="row ">
      <div class="container ">
        <div class="row">

          <div class="col-xs-12 top105">
            <div class="descricao-lojas ">
              <!-- mapa -->
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3838.3227755811063!2d-47.96882404926677!3d-15.839623988969235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a2e284ffa1e37%3A0xf8af8c6086739b4b!2zUGl6emEgQ8Opc2FyIC0gR3VhcsOh!5e0!3m2!1spt-BR!2sbr!4v1448659002735" width="1130" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
              <!-- mapa -->

              <h5 class="top60">Guará</h5>
              <h6 class="top20">Qi 33 s/n bl A lj 10  - Guará -  (61) 3567-3030</h6>
              <h1 class="top30">HORÁRIO DE FUNCIONAMENTO</h1>
              <h2 class="top40">DOM: 11:30:00 - 23:30:00</h2>
              <h2>SEG: 17:00:00 - 23:30:00</h2>
              <h2>TER: 17:00:00 - 23:30:00</h2>
              <h2>QUA: 17:00:00 - 23:30:00</h2>
              <h2>QUI: 17:00:00 - 23:30:00</h2>
              <h2>SEX: 12:00:00 - 23:59:00</h2>
              <h2>SAB: 11:30:00 - 23:59:00</h2>
            </div>


            <!-- novidades -->
            <div class=' col-xs-12 top60 carroucel-novidades1 text-center'>

              <!-- novidades 01 -->
              <div class="item  active">
                <div class="row">
                  <!-- novidade 01 -->
                  <div class="col-xs-4">
                    <div class="thumbnail">
                      <a href="#">
                      <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                      </a>
                      <h1 class="top20">Sobradinho</h1>
                      <h4 class="top20">Q 2 Conj D 3 s/n cj A lt 3/5 loja 8</h4>
                      <h6>(61) 3567-3030</h6>
                      <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                    </div>
                  </div>


                  <!-- novidade 02 -->
                  <div class="col-xs-4">
                    <div class="thumbnail">
                      <a href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                      </a>
                      <h1 class="top20">Sudoeste</h1>
                      <h4 class="top20">ClSW 302 Bl B s/n LJ75</h4>
                      <h6>(61) 3341-1001</h6>
                      <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                    </div>
                  </div>  


                  <!-- novidade 03 -->
                  <div class="col-xs-4">
                    <div class="thumbnail">
                      <a href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                      </a>
                      <h1 class="top20">Águas Claras</h1>
                      <h4 class="top20">Rua 07 Norte Max Mall loja 18 </h4>
                      <h6>(61) 3877-22001</h6>
                      <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                </div>
              </div>




              <!-- novidade 04 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Asa Sul</h1>
                  <h4 class="top20">CLS 404 Bloco A Loja 01 </h4>
                  <h6>(61) 3242-2221</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                </div>
              </div>


              <!-- novidade 05 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Lago Sul</h1>
                  <h4 class="top20">SHIS Qi 17 s/n bl G lj 102, Brasília</h4>
                  <h6>(61) 3364-0191</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                </div>
              </div>  


              <!-- novidade 06 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Taguatinga</h1>
                  <h4 class="top20">QNA 17, lote 13 Taguatinga Norte</h4>
                  <h6>(61) 3352-0500</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>

              <!-- novidade 07 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Gama</h1>
                  <h4 class="top20">Q 30 s/n lt 48 </h4>
                  <h6>(61) 3384-0202</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>

              <!-- novidade 07 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Gama</h1>
                  <h4 class="top20">Q 30 s/n lt 48 </h4>
                  <h6>(61) 3384-0202</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>




            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- QUAIS AS LOJAS -->







  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->





</body>

</html>
