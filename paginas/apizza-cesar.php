<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>



<body class="bg-pizza-cesar">



  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->

  <!-- menu home  -->
  <div class="container-fluid bg-pizza-descricao ">
    <div class="row ">
      <div class="container ">
        <div class="row">

        <div class="col-xs-12 top125">
            <div class="descricao-pizza ">
              <p>Tradicional no mercado gastronômico de Brasília e Goiânia, a nossa marca
               é especializada no preparo e comercialização de Pizzas e demais pratos da 
               apreciada culinária italiana. Com foco na dedicação absoluta à qualidade de
               nossos produtos. O prestígio adquirido, no curso das nossas atividades, decorre
               do empenho incansável e no profissionalismo de nossa equipe na arte do bem servir.
               As nossas centrais Delivery encontram-se devidamente equipadas para sempre 
               prestar atendimento personalizado a nossa clientela.
             </p>
             <h5>Rodízio</h5>
             <p>O melhor e mais completo rodízio da cidade. Traga sua família e venha se deliciar com mais 
               de 72 sabores de pizzas. As nossas centrais Delivery encontram-se devidamente equipadas para sempre
               prestar atendimento personalizado a nossa clientela
             </p>

             <h5>Missão</h5>
             <p>Buscar superar as expectativas dos nossos clientes, oferecendo um cardápio diferenciado pela excelência dos seus produtos e
              pela sua variedade, despertando proveitosos momentos de descontração, bem estar e comodidade, cercados pela eficiência dos nossos serviços.
            </p>

            <h5>Visão</h5>
            <p>Levar a marca Pizza Cesar a todo território nacional.</p>

            <h5>Valores</h5>
            <p>A nossa conduta empresarial, inspirada aos nossos clientes, baseia-se
              na honestidade, confiabilidade, criatividade e excelência.
            </p>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
<!-- menu home -->







<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->





</body>

</html>
