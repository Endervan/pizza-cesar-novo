<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



  <!-- Media Slider Carousel BS3   -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('#media').carousel({
        pause: true,
        interval: 4000,
      });
    });</script>
    <!-- Media Slider Carousel BS3   -->


    <!-- Media Slider Carousel BS3   -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('#media1').carousel({
          pause: true,
          interval: 4000,
        });
      });</script>
      <!-- Media Slider Carousel BS3   -->



      <script>
      $(document).ready(function() {
        $('#myModal').modal('show');
      });
      </script>


    </head>



    <body class="bg-home">

      <div class="banner-home">
        <div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content text-center top50">    
            <button type="button" class="close" style="position: absolute; right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; Fechar</span></button>        
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/bannersite.jpg" alt="">
            </div>
          </div>
        </div>
      </div>





      <!-- topo -->
      <?php require_once('./includes/topo.php') ?>
      <!-- topo -->

      <!-- menu home  -->
      <div class="container-fluid bg-menu ">
        <div class="row ">
          <div class="container ">
            <div class="row">

              <div class="col-xs-12 top75">
                <div class="categorias-home ">

                  <!-- menu -->
                  <nav class="navbar navbar-default top35" role="navigation">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                     <ul class="nav navbar-nav">

                      <li class="active">
                       <a href="">
                         <img class="bottom30" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-cardapio.png" alt=""> <br>
                         CARDÁPIO  <br>
                         <img class="top30"  src="<?php echo Util::caminho_projeto() ?>/imgs/rolo.png" alt="">
                         <h2 class="top30">São mais de <br>70 sabores <br>à sua escolha.</h2>
                         
                       </a>
                     </li>

                     <li>
                       <a href="">
                         <img class="bottom30" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-rodizio.png" alt=""> <br>
                         RODÍZIO  <br>
                         <img class="top30"  src="<?php echo Util::caminho_projeto() ?>/imgs/rolo.png" alt="">
                         <h2 class="top30">O melhor</h2>
                         <h2>rodízio</h2>
                         <h2>da cidade</h2>
                       </a>
                     </li>

                     <li>
                       <a href="">
                         <img class="bottom30" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-delivery.png" alt=""> <br>
                         DELIVERY  <br>
                         <img class="top30"  src="<?php echo Util::caminho_projeto() ?>/imgs/rolo.png" alt="">
                         <h2 class="top30">Entrega rápida</h2>
                         <h2>e sua pizza</h2>
                         <h2>quentinha</h2>
                       </a>
                     </li>

                     <li>
                       <a href="">
                         <img class="bottom30" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-promocoes.png" alt=""> <br>
                         PROMOÇÕES  <br>
                         <img class="top30"  src="<?php echo Util::caminho_projeto() ?>/imgs/rolo.png" alt="">
                         <h2 class="top30">Todos os dias</h2>
                         <h2>uma promoção</h2>
                         <h2>especial</h2>
                       </a>
                     </li>


                   </ul>
                 </div>
                 <!-- /.navbar-collapse -->
               </nav>    
             </div>
             <!-- menu topo-->  
           </div>

           <!-- descricao menu -->
           <div class="col-xs-12 categorias-home">
             <p>Tradicional no mercado gastronômico de Brasília e Goiânia, a nossa marca é especializada no preparo e comercialização de Pizzas
              e demais pratos da apreciada culinária italiana. Com foco na dedicação absoluta à qualidade de nossos produtos. O prestígio 
              adquirido, no curso das nossas atividades, decorre do empenho incansável e no profissionalismo de nossa equipe na arte do bem servir.
              As nossas centrais Delivery encontram-se devidamente equipadas para sempre prestar atendimento personalizado a nossa clientela.
            </p>
          </div>
          <!-- descricao menu -->
        </div>
      </div>
    </div>
  </div>
  <!-- menu home -->




  <!-- 72 sabores  -->
  <div class="container-fluid sabores">
    <div class="row ">
      <div class="container ">
        <div class="row">

          <div class="col-xs-12 text-center top170">
            <a href="">
              <img  src="<?php echo Util::caminho_projeto() ?>/imgs/75sabores.png" alt="">
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- 72 sabores  -->

  <!-- as mais premiadas -->
  <div class="container-fluid seta premiadas ">
    <div class="row">
      <div class="container">

        <div class='row'>
          <div class='col-xs-12 top295 carroucel-premiadas'>
            <div class="carousel slide media-carousel" id="media">
              <div class="carousel-inner">

                <div class="item  active">
                  <div class="row">
                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>

                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>          


                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>          


                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>          


                  </div>
                </div>

                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>

                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>          


                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>          


                    <div class="col-xs-3">
                      <a class="thumbnail" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                      <a class="thumbnail" href="#">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/premiadas01.jpg" alt="">
                      </a>
                    </div>          


                  </div>
                </div>





              </div>
              <a data-slide="prev" href="#media" class="left carousel-control"><img src="<?php echo Util::caminho_projeto() ?>/imgs/seta-esquerda.png" alt=""></a>
              <a data-slide="next" href="#media" class="right carousel-control"><img src="<?php echo Util::caminho_projeto() ?>/imgs/seta-direita.png" alt=""></a>
            </div>                          
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- as mais premiadas -->



  <!-- lojas  -->
  <div class="container-fluid lojas">
    <div class="row ">
      <div class="container ">
        <div class="row">

          <div class="col-xs-12 text-center top160">
            <a href="">
              <img  src="<?php echo Util::caminho_projeto() ?>/imgs/12lojas.png" alt="">
              <h3>Encontre a Pizza Cesar mais próxima de você!</h3>
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- lojas  -->


  <!-- novidades home -->
  <div class="container-fluid seta1 novidades ">
    <div class="row">
      <div class="container">

        <div class='row'>
          <div class='col-xs-12 top150 carroucel-novidades text-center'>
            <div class="carousel slide media-carousel" id="media1">
              <div class="carousel-inner">

                <!-- novidades 01 -->
                <div class="item  active">
                  <div class="row">
                    <!-- novidade 01 -->
                    <div class="col-xs-4">
                      <div class="thumbnail">
                        <a href="#">
                          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/novidades-home01.jpg" alt="">
                        </a>
                        <h1 class="top20">Faça seu aniversário aqui</h1>
                        <h4 class="top20">No Seu Aniversário quem dá o presente é a Pizza Cesar. Comemore seu aniversário saboreando a pizza mais famosa da cidade e o rodízio do </h4>

                        <a class="btn btn-saiba-mais top40 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                      </div>
                    </div>


                    <!-- novidade 02 -->
                    <div class="col-xs-4">
                      <div class="thumbnail">
                        <a href="#">
                          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/novidades-home02.jpg" alt="">
                        </a>
                        <h1 class="top20">Solicitação de Reserva Online</h1>
                        <h4 class="top20">Vamos organizar uma festinha de aniversário, uma confraternização, ou um happy hour com os amigos? Agora você pode fazer sua reserva on-line e </h4>

                        <a class="btn btn-saiba-mais top40 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                      </div>
                    </div>  


                    <!-- novidade 03 -->
                    <div class="col-xs-4">
                      <div class="thumbnail">
                        <a href="#">
                          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/novidades-home03.jpg" alt="">
                        </a>
                        <h1 class="top20">Traga sua Turma</h1>
                        <h4 class="top20">Nos amamos receber suas confraternizações, seja ela qual for. Venha fazer a festa na Pizza Cesar, esperamos por você.</h4>

                        <a class="btn btn-saiba-mais top40 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                      </div>
                    </div>
                  </div>
                </div>


                
                <!-- novidades 02 -->
                <div class="item">
                  <div class="row">
                    <!-- novidade 01 -->
                    <div class="col-xs-4">
                      <div class="thumbnail">
                        <a href="#">
                          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/novidades-home01.jpg" alt="">
                        </a>
                        <h1 class="top20">Faça seu aniversário aqui</h1>
                        <h4 class="top20">No Seu Aniversário quem dá o presente é a Pizza Cesar. Comemore seu aniversário saboreando a pizza mais famosa da cidade e o rodízio do </h4>

                        <a class="btn btn-saiba-mais top40 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                      </div>
                    </div>


                    <!-- novidade 02 -->
                    <div class="col-xs-4">
                      <div class="thumbnail">
                        <a href="#">
                          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/novidades-home02.jpg" alt="">
                        </a>
                        <h1 class="top20">Solicitação de Reserva Online</h1>
                        <h4 class="top20">Vamos organizar uma festinha de aniversário, uma confraternização, ou um happy hour com os amigos? Agora você pode fazer sua reserva on-line e </h4>

                        <a class="btn btn-saiba-mais top40 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                      </div>
                    </div>  


                    <!-- novidade 03 -->
                    <div class="col-xs-4">
                      <div class="thumbnail">
                        <a href="#">
                          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/novidades-home03.jpg" alt="">
                        </a>
                        <h1 class="top20">Traga sua Turma</h1>
                        <h4 class="top20">Nos amamos receber suas confraternizações, seja ela qual for. Venha fazer a festa na Pizza Cesar, esperamos por você.</h4>

                        <a class="btn btn-saiba-mais top40 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                      </div>
                    </div>
                  </div>
                </div>




              </div>
              <a data-slide="prev" href="#media1" class="left carousel-control"><img src="<?php echo Util::caminho_projeto() ?>/imgs/seta-esquerda.png" alt=""></a>
              <a data-slide="next" href="#media1" class="right carousel-control"><img src="<?php echo Util::caminho_projeto() ?>/imgs/seta-direita.png" alt=""></a>
            </div>                          
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- novidades home -->


  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->





</body>

</html>
