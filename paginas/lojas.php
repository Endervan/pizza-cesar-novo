<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>



<body class="bg-lojas">



  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->

  <!-- QUAIS AS LOJAS  -->
  <div class="container-fluid bg-lojas-descricao ">
    <div class="row ">
      <div class="container ">
        <div class="row">

            <!-- novidades -->
            <div class=' col-xs-12 top60 bottom80 carroucel-novidades1 text-center'>
              
                <div class="row">
                  <!-- novidade 01 -->
                  <div class="col-xs-4">
                    <div class="thumbnail">
                      <a href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                      </a>
                      <h1 class="top20">Sobradinho</h1>
                      <h4 class="top20">Q 2 Conj D 3 s/n cj A lt 3/5 loja 8</h4>
                      <h6>(61) 3567-3030</h6>
                      <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                    </div>
                  </div>


                  <!-- novidade 02 -->
                  <div class="col-xs-4">
                    <div class="thumbnail">
                      <a href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                      </a>
                      <h1 class="top20">Sudoeste</h1>
                      <h4 class="top20">ClSW 302 Bl B s/n LJ75</h4>
                      <h6>(61) 3341-1001</h6>
                      <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                    </div>
                  </div>  


                  <!-- novidade 03 -->
                  <div class="col-xs-4">
                    <div class="thumbnail">
                      <a href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                      </a>
                      <h1 class="top20">Águas Claras</h1>
                      <h4 class="top20">Rua 07 Norte Max Mall loja 18 </h4>
                      <h6>(61) 3877-22001</h6>
                      <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                

              <!-- novidade 04 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Asa Sul</h1>
                  <h4 class="top20">CLS 404 Bloco A Loja 01 </h4>
                  <h6>(61) 3242-2221</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                </div>
              </div>


              <!-- novidade 05 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Lago Sul</h1>
                  <h4 class="top20">SHIS Qi 17 s/n bl G lj 102, Brasília</h4>
                  <h6>(61) 3364-0191</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a>
                </div>
              </div>  


              <!-- novidade 06 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Taguatinga</h1>
                  <h4 class="top20">QNA 17, lote 13 Taguatinga Norte</h4>
                  <h6>(61) 3352-0500</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>

              <!-- novidade 07 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Gama</h1>
                  <h4 class="top20">Q 30 s/n lt 48 </h4>
                  <h6>(61) 3384-0202</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>

              <!-- novidade 08 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Gama</h1>
                  <h4 class="top20">Q 30 s/n lt 48 </h4>
                  <h6>(61) 3384-0202</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>


              <!-- novidade 09 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Gama</h1>
                  <h4 class="top20">Q 30 s/n lt 48 </h4>
                  <h6>(61) 3384-0202</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>

              <!-- novidade 10 -->
              <div class="col-xs-4">
                <div class="thumbnail">
                  <a href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/lojas01.jpg" alt="">
                  </a>
                  <h1 class="top20">Gama</h1>
                  <h4 class="top20">Q 30 s/n lt 48 </h4>
                  <h6>(61) 3384-0202</h6>
                  <a class="btn btn-saiba-mais top15 " href="#" role="button">SAIBA<i class="fa fa-plus"></i></a> 
                </div>
              </div>

          
            </div>


         
        </div>
      </div>
    </div>
  </div>
  <!-- QUAIS AS LOJAS -->







  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->





</body>

</html>
