<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-servicos -->
  <div class="container bg-servicos">
    <div class="row"></div>
  </div>
  <!-- bg-servicos -->


  <!-- servicos descricao  -->
  <div class="container top25">
    <div class="row">
      <div class="col-xs-12 servicos-descricao">
        <h6>CONSTRUÇÃO E REFORMA DE PISCINAS</h6>
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/servicos01.jpg" alt="">
        <p class="bottom10">A BSB Piscinas é uma empresa especializada na construção e reformas 
          de piscinas em vinil, pastilhas de cerâmica e vidro e azulejos paginados.
          Com a garantia de ter a realização de seu projeto com total segurança e 
          qualidade, sempre utilizando os materiais mais adequados e zelandopela
          eficiência em cada etapa do processo.
        </p>
       <div class="pull-right">
          <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-orcamentos.jpg" alt="">
        </a>
       </div>

       
        <h6 class="top30">EQUIPAMENTOS E ASSISTÊNCIA TÉCNICA</h6>
      </div>

      <!-- tipos de assitencia -->
      <div class="col-xs-5">
       <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/servicos02.png" alt="">
     </div>

     <div class="col-xs-7 tipos-servicos bottom20">
      <h5 class="top30">A BSB Piscinas oferece a todos, uma prestação
        de serviços com mão-de-obra especializada na:
      </h5>

      <p class="top25">instalação de equipamentos em geral,</p>

      <p class="top25">assistência técnica,</p>

      <p class="top25">instalação de aquecimento de piscinas,</p>

      <p class="top25">venda e instalação de saunas,</p>

      <p class="top25">instalação de acessórios, duchas e cascatas,</p>

      <p class="top25">venda de produtos químicos em geral e muito mais.</p>


    </div>
    <!-- tipos de assitencia -->

    <div class="top15 bottom100 pull-right">
      <a href="">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-orcamentos.jpg" alt="">
      </a>
    </div>







  </div>
</div>
<!-- empresa descricao  -->






</div>
<!-- nosso parceiros -empresa -->


<?php require_once('../includes/rodape.php'); ?>


</body>

</html>