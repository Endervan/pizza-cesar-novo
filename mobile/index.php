<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: false,
        itemWidth: 141,
        itemMargin: 7,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>


  <script>
  $(document).ready(function() {
    $('#myModal').modal('show');
  });
  </script>


</head>

<body>

    <div class="banner-home">
        <div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content text-center top50">    
            <button type="button" class="close" style="position: absolute; right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; Fechar</span></button>        
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/bannermobile.jpg" class="input100" alt="">
            </div>
          </div>
        </div>
      </div>



  <?php require_once('./includes/topo.php'); ?>



  <!-- slider -->
  <div class="container">
    <div class="row slider-index">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-home.jpg" alt="">
            <div class="carousel-caption">
              <a href="">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>


          <div class="item">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-home.jpg" alt="">
            <div class="carousel-caption">
              <a href="">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>


          <div class="item">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-home.jpg" alt="">
            <div class="carousel-caption">
              <a href="">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="item">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-home.jpg" alt="">
            <div class="carousel-caption">
              <a href="">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>


        </div>


        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> 


      </div>
    </div>
  </div>
  <!-- slider -->





  <!-- flex slider -->
  <div class="container">
    <div class="row slider-prod">
      <div class="col-xs-12">
        <h4>NOSSAS CATEGORIAS:</h4>
        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider text-center">
          <ul class="slides lista-categorias">

            <li>
              <a href="" >
               <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
               <h5>PISCINAS</h5> 
             </a>
           </li>

           <li >
            <a href="" >
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
             <h5>PISCINAS</h5> 
           </a>
         </li>


         <li>
          <a href="" >
           <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
           <h5>PISCINAS</h5> 
         </a>
       </li>

       <li>
        <a href="" >
         <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
         <h5>PISCINAS</h5> 
       </a>
     </li>





     <!-- items mirrored twice, total of 12 -->
   </ul>

   <!-- seta personalizadas -->
   <div class="custom-navigation text-right">
    <a href="#" class="flex-prev"><i class="fa fa-chevron-left"></i>
    </a>
    
    
    <a href="#" class="flex-next"><i class="fa fa-chevron-right"></i>
    </a>

  </div>
  <!-- seta personalizadas -->
</div>




</div>
</div>
</div>
<!-- flex slider -->


<!-- produtos home  -->
<div class="container top25">
  <div class="row">
    <div class="col-xs-12">
      <h2>DESTAQUES</h2>
    </div>

    <!-- produto 01 -->
    <div class="col-xs-6 top20">
      <div class="thumbnail produtos-home ">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home01.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 02 -->
    <div class="col-xs-6 top20">
      <div class="thumbnail produtos-home ">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 03 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home01.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>


    <!-- produto 04 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- botao produto -->
    <div class="col-xs-12 text-center top25">
      <a href="#" class="btn btn-primary" role="button">VER PRODUTOS</a>
    </div>
    <!-- botao produto -->


  </div>
</div>
<!-- produtos home  -->


<!-- conheca mais bsb -->
<div class="container top45 conheca-mais">
  <div class="row">
    <div class="col-xs-12 conheca">
      <h4 class="top20">CONHEÇA UM POUCO MAIS A BSB PISCINAS</h4>
      <h5 class="top20">A BSB Piscinas oferece construção e reformas de piscinas de azulejos paginados, 
        em vinil ou de pastilhas de cerâmica e vidro.
      </h5>
      <h5 class="top25">Instalação de equipamentos em geral, assistência técnica especializada, aquecimen
        to de piscinas, vendas e instalação de saunas, equipamentos e acessórios, duchas e
        cascatas, produtos químicos em geral e muito mais.
      </h5>
    </div>
  </div>
</div>
<!-- conheca mais bs  -->


<!-- nossas dicas home -->
<div class="container top30 bottom30">
  <div class="row">
    <div class="col-xs-12 dicas-home">
      <h4 class="top20">NOSSAS DICAS</h4>
    </div>
    <!-- dica 01 -->
    <div class=" col-xs-12 dicas-home1 top30 pbottom20">
      <div class="borda-azul">
        <a href="">
          <div class="col-xs-4">
           <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/dica-home01.jpg" alt="" class="img-circle">
         </div>

         <div class=" col-xs-8">
          <h4>A MELHORES PRATICAS DE CONSTRUÇÃO DE PISCINAS</h4>
          <h5>A BSB Piscinas oferece construão e refor mas de piscinas de azulejos paginados, em vinil ou</h5>
        </div>
      </a>
    </div>  
  </div>

   <!-- dica 02 -->
    <div class=" col-xs-12 dicas-home1 top30 pbottom20">
      <div class="borda-azul">
        <a href="">
          <div class="col-xs-4">
           <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/dica-home01.jpg" alt="" class="img-circle">
         </div>

         <div class=" col-xs-8">
          <h4>A MELHORES PRATICAS DE CONSTRUÇÃO DE PISCINAS</h4>
          <h5>A BSB Piscinas oferece construão e refor mas de piscinas de azulejos paginados, em vinil ou</h5>
        </div>
      </a>
    </div>  
  </div>

</div>
</div>
<!-- nossas dicas home -->


<?php require_once('./includes/rodape.php'); ?>


</body>

</html>