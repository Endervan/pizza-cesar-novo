<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-orcamento -->
  <div class="container">
    <div class="row">
      <div class="faleConosco">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.729162581955!2d-47.97135358455934!3d-15.870825829068952!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a2e59437b3569%3A0x3f2e953cec8c1a02!2sBSB+Piscinas!5e0!3m2!1spt-BR!2sbr!4v1447856870469" width="480" height="379" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

      <div class="col-xs-6 telefone-topo1 top20">
        <h4>
          (61) 3552-1121
          <a href="tel:6135249876" class="btn btn-azul">
            CHAMAR
          </a>

        </h4>
      </div>

      <div class="col-xs-6 telefone-topo1 top20">
        <h4>
          (61) 3552-1121
          <a href="tel:6135249876" class="btn btn-azul">
            CHAMAR
          </a>

        </h4>
      </div>




      <!-- contatos topo  -->

      <div class="col-xs-12 top20">
      <h2>FALE CONOSCO</h2>
        <!-- formulario orcamento -->
        <div class="FormContato top20 bottom80">
          <div class="row">
            <div class="col-xs-6 form-group ">
              <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
              <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
            </div>

            <div class="col-xs-6 form-group ">
              <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
              <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
            </div>
          </div>

          <div class="row">
            <div class="col-xs-6 form-group">
              <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
              <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
            </div>

            <div class="col-xs-6 form-group">
             <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
             <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
           </div>
         </div>

         <div class="row">
          <div class="col-xs-12 form-group">
            <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
            <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="text-right  top15">
          <button type="submit" class="btn btn-azul1" name="btn_contato">
            ENVIAR
          </button>
        </div>
      </div>
      <!-- formulario orcamento -->
    </div>



  </div>
</div>
<!-- bg-orcamento -->





<?php require_once('../includes/rodape.php'); ?>


</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>