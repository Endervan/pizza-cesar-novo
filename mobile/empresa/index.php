<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

  <!-- ---- LAYER SLIDER ---- -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage: 1,
        scrollbar: true,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: false,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->

</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-empresa -->
  <div class="container bg-empresa">
    <div class="row"></div>
  </div>
  <!-- bg-empresa -->


  <!-- empresa descricao  -->
  <div class="container top25">
    <div class="row">
      <div class="col-xs-12">
        <h2>A EMPRESA</h2>
      </div>

      <div class="col-xs-12 top20">
        <div class="jumbotron empresa-descricao">

          <p class="top20 bottom40">A BSB Piscinas oferece construção e reformas de piscinas de azulejos pa
            ginados, em vinil ou de pastilhas de cerâmica e vidro.        
            Instalação de equipamentos em geral, assistência técnica especializada, 
            aquecimento de piscinas, vendas e instalação de saunas, equipamentos
            e acessórios, duchas e cascatas, produtos químicos em geral e muito 
            mais.


            A empresa trabalha com profissionais experientes e altamente qualifica
            dos para melhor atendê-lo.
          </p>

        </div>
      </div>

      


    </div>
  </div>
  <!-- empresa descricao  -->



  <!-- depoimento -->
  <div class="container top10">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="bottom20">DEPOIMENTO</h2>
      </div>

      <div id="carousel-example-generic" class="carousel slide top20 bottom120 text-center" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner personalizar-carroucel" role="listbox">
          <!-- item 01 -->
          <div class="item active">
            <div class="col-xs-5">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/depoimento01.jpg" alt="">
            </div>
            <div class="col-xs-7">
              <p>
                A BSB Piscinas oferece construção e reformas de piscinas de azulejos paginados, em vinil 
                ou de pastilhas de cerâmica e vidro. Instalação de equipamentos em geral, assistência técnica 
                especializada, aquecimen to de piscinas, vendas e instalação de saunas, equipamentos e aces sórios,
                duchas e cascatas, produtos químicos em geral e muito mais
              </p>
              <h4 class="top10">João Alves</h4>
            </div>
            <div class="carousel-caption">
            </div>
          </div>

          <!-- item 02 -->
          <div class="item">
            <div class="col-xs-5">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/depoimento01.jpg" alt="">
            </div>
            <div class="col-xs-7">
              <p>
                A BSB Piscinas oferece construção e reformas de piscinas de azulejos paginados, em vinil ou de 
                pastilhas de cerâmica e vidro. Instalação de equipamentos em geral, assistência técnica especializada,
                aquecimen to de piscinas, vendas e instalação de saunas, equipamentos e aces sórios, duchas e cascatas,
                produtos químicos em geral e muito mais
              </p>
              <h4 class="top10">Maria e Paulo</h4>
            </div>
            <div class="carousel-caption">
            </div>
          </div>


          <!-- item 03 -->
          <div class="item ">
            <div class="col-xs-5">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/depoimento01.jpg" alt="">
            </div>
            <div class="col-xs-7">

              <p>
                A BSB Piscinas oferece construção e reformas de piscinas de azulejos paginados, em vinil ou 
                de pastilhas de cerâmica e vidro. Instalação de equipamentos em geral, assistência técnica
                especializada, aquecimen to de piscinas, vendas e instalação de saunas, equipamentos e aces sórios,
                duchas e cascatas, produtos químicos em geral e muito mais
              </p>
              <h4 class="top10">Gabriella</h4>
            </div>
            <div class="carousel-caption">
            </div>
          </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control posicao-caroucel controle-esquerdo" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control posicao-caroucel  controle-direito" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>


      </div>
      
    </div>
  </div>
  <!-- depoimento -->


  <!-- portifolio empresa -->
  <div class="container  fundo-azul pb25">
    <div class="row">
      <div class="col-xs-12 portifolio-home">
        <h2 class="bottom20 top25">NOSSO PORTFÓLIO</h2>
      </div>

      <!-- portifolio01 -->
      <div class="col-xs-6">
        <a href="">
          <img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/portifolio-empresa01.jpg" alt="" class="input100">
        </a>
      </div>

      <!-- portifolio02 -->
      <div class="col-xs-6">
        <a href="">
          <img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/portifolio-empresa01.jpg" alt="" class="input100">
        </a>
      </div>

      <!-- portifolio03 -->
      <div class="col-xs-6">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/portifolio-empresa01.jpg" alt="" class="input100">
        </a>
      </div>

      <!-- portifolio04 -->
      <div class="col-xs-6">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/portifolio-empresa01.jpg" alt="" class="input100">
        </a>
      </div>


    </div>
  </div>
  <!-- portifolio empresa -->


  <!-- nosso parceiros -empresa -->
  <div class="container">
    <div class="row ">
    <div class="col-xs-12 top30">
        <div class="nossas-dicas-empresa text-center">
          <h3>NOSSOS PARCEIROS</h3>
        </div>
      </div>
    </div>

    <!-- slider touchcaroroucel -->
    <div class="row personalizar-slider">
      <div id="carousel-gallery" class="touchcarousel  black-and-white">

        <ul class="touchcarousel-container pbottom40">

          <li class="touchcarousel-item ">
            <a href="">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/pardeiros-empresa.png" alt="" class="img-circle">
            </a>
          </li>

          <li class="touchcarousel-item ">
            <a href="">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/pardeiros-empresa.png" alt="" class="img-circle">
            </a>
          </li>

          <li class="touchcarousel-item ">
            <a href="">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/pardeiros-empresa.png" alt="" class="img-circle">
            </a>
          </li>

          <li class="touchcarousel-item ">
            <a href="">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/pardeiros-empresa.png" alt="" class="img-circle">
            </a>
          </li>

          <li class="touchcarousel-item ">
            <a href="">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/pardeiros-empresa.png" alt="" class="img-circle">
            </a>
          </li>
        </ul>

      </div>

    </div>
    <!-- slider touchcaroroucel -->


  </div>
  <!-- nosso parceiros -empresa -->


  <?php require_once('../includes/rodape.php'); ?>


</body>

</html>