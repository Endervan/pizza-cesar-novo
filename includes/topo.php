<!--contatos-topo -->
<div class="container-fluir fundo-transparente">
	<div class="row">

		<div class="container">
			<div class="row">
				<!-- logo -->
				<div class="col-xs-2 top35">
					<div class="topo">
						<a href="<?php echo Util::caminho_projeto() ?>/">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
						</a>
					</div>
				</div>
				<!-- logo -->

				<!-- icon redes socias -->
				<div class="col-xs-3 top125 left30 redes">

					<span>
						<a href="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-redes01.png" alt="">
						</a>
					</span>

					<span>
						<a href="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-redes02.png" alt="">
						</a>
					</span>

					<span>
						<a href="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-redes03.png" alt="">
						</a>
					</span>

					
				</div>
				<!-- icon redes socias -->


				<!-- peca online -->
				<div class="col-xs-3 online text-center top45">
					<a href="">
						<h3>Peça pela</h3>
						<h3>internet</h3>
					</a>
				</div>
				<!-- peca online -->

				<!-- reserva online -->
				<div class="col-xs-3 reserva text-center left20 top30">
					<a href="">
						<h3 class="top20">Faça sua</h3>
						<h3>reserva</h3>
					</a>
				</div>

				<!-- reserva online -->
			</div>


		</div>
	</div>
</div> 

<!-- contatos-topo -->


<!-- menu  -->

<div class="container descer-menu">
	<div class="row">
		<div class="col-xs-12 navs-personalizados fonte-nove">
			<!-- menu -->
			<nav class="navbar navbar-default navbar-left" role="navigation">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">

						<li class="active">
							<a href="<?php echo Util::caminho_projeto() ?>/">
								HOME
							</a>
						</li>

						<li >
							<a href="<?php echo Util::caminho_projeto() ?>/cardapio">
								CARDÁPIO
							</a>
						</li>

						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/apizza-cesar">

								A PIZZA CESAR
							</a>
						</li>

						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/promocoes">

								PROMOÇÕES
							</a>
						</li>

						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/lojas">

								LOJAS
							</a>
						</li>

						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/franquias">

								FRANQUIAS
							</a>
						</li>

						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/reservas">

								RESERVAS
							</a>
						</li>

						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">

								FALE CONOSCO
							</a>
						</li>

					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</nav>    
		</div>
	</div>
</div>

<!-- menu -->




