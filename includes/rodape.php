

<!-- menu  -->

<div class="container-fluid rodape">
	<div class="row">
		<div class="container ">
			<div class="row">
			<a href="#" class="scrollup">scrollup</a>
				<div class="col-xs-12 navs-personalizados1 fonte-nove top30">
					<!-- menu -->
					<nav class="navbar navbar-default navbar-left" role="navigation">
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-ex1-collapse">
							<ul class="nav navbar-nav">

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/">
										HOME
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/cardapio">
										CARDÁPIO
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/apizza-cesar">

										A PIZZA CESAR
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/promocoes">

										PROMOÇÕES
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/lojas">

										LOJAS
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/franquias">

										FRANQUIAS
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/reservas">

										RESERVAS
									</a>
								</li>

								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">

										FALE CONOSCO
									</a>
								</li>

							</ul>
						</div>
						<!-- /.navbar-collapse -->
					</nav>    
				</div>

				<!-- logo -->
				<div class="col-xs-3 top60">
					<a href="<?php echo Util::caminho_projeto() ?>/">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png"  alt="">
					</a>
				</div>
				<!-- logo -->

				<!-- contatos -->
				<div class="col-xs-1 rodape-contatos fonte-nove top50 text-center right45">
					<div class="thumbnail">
						<h1 class="pt5">águas claras</h1>
						<h4 >3877.2200</h4>
					</div>
					<div class="thumbnail">
						<h1 class="pt5">sobradinho</h1>
						<h4 >3487.6262</h4>
					</div>
				</div>

				<div class="col-xs-1 rodape-contatos fonte-nove top50 text-center right45">
					<div class="thumbnail">
						<h1 class="pt5">asa norte</h1>
						<h4 >3242.2221</h4>
					</div>
					<div class="thumbnail">
						<h1 class="pt5">samambaia</h1>
						<h4 >3352.0500</h4>
					</div>
				</div>

				<div class="col-xs-1 rodape-contatos fonte-nove top50 text-center right45">
					<div class="thumbnail">
						<h1 class="pt5">sudoeste</h1>
						<h4 >33341.1001</h4>
					</div>
					<div class="thumbnail">
						<h1 class="pt5">asa sul</h1>
						<h4 >3242.2221</h4>
					</div>
				</div>

				<div class="col-xs-1 rodape-contatos fonte-nove top50 text-center right45">
					<div class="thumbnail">
						<h1 class="pt5">noroeste</h1>
						<h4 >3341.1001</h4>
					</div>
					<div class="thumbnail">
						<h1 class="pt5">gama</h1>
						<h4 >3384.0202</h4>
					</div>
				</div>

				<div class="col-xs-1 rodape-contatos fonte-nove top50 text-center right45">
					<div class="thumbnail">
						<h1 class="pt5">taguatinga</h1>
						<h4 >3352.0500</h4>
					</div>
					<div class="thumbnail">
						<h1 class="pt5">guará</h1>
						<h4 >3567.3030</h4>
					</div>
				</div>

				<div class="col-xs-1 rodape-contatos fonte-nove top50 text-center right45">
					<div class="thumbnail">
						<h1 class="pt5">ceilândia</h1>
						<h4 >3352.0500</h4>
					</div>
					<div class="thumbnail">
						<h1 class="pt5">lago sul</h1>
						<h4 >3364.0191</h4>
					</div>
				</div>
				<!-- contatos -->

				<!-- icon redes socias -->
				<div class="col-xs-2 col-xs-offset-10 top20 redes">

					<span>
						<a href="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-redes01.png" alt="">
						</a>
					</span>

					<span>
						<a href="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-redes02.png" alt="">
						</a>
					</span>

					<span>
						<a href="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-redes03.png" alt="">
						</a>
					</span>

					
				</div>
				<!-- icon redes socias -->
			</div>
		</div>

		<div class="rodape-preto top50">
			<h5 class="text-center">Copyright Pizza Cesar 2015 - Todos o Direitos Reservados</h5>
		</div>

	</div>
</div>
<!-- menu -->
